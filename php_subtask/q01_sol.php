<?php
/*
* INFO/CS 1300
* Fall 2016
*
* Assignment 8, question 1
*
*/

// variables
$i; // counter variable
$key; // holder variable for array
$value; // holder variable for array
$plain_array = array(1, 2, 3, "four");
$assoc_array = array("Brady" => 12, "Blount" => 29, "Garropolo" => 10);

print "Loop 1: <br>";
for ($i = 0; $i <= (count($plain_array)) - 1; $i++) {
    echo ("i: $plain_array[$i] <br>");
}

print "<br>";
print "<br>";
print "<br>";

print "Loop 2: <br>";
foreach ($plain_array as $value) {
    echo ("value: $value <br>");
}

print "<br>";
print "<br>";
print "<br>";

print "Loop 3: <br>";
foreach ($plain_array as $value) {
    if (is_int($value) === true) {
        echo ("value: $value <br>");
    }
    elseif (is_string($value) === true) {
    }
}

print "<br>";
print "<br>";
print "<br>";

print "Loop 4: <br>";
foreach ($assoc_array as $value) {
    echo (array_search($value, $assoc_array) . ": $value <br>");
}
?>

